# MMS-Monitor

## Purpose

This project is aimed at people using a Gnu/Linux phone and a cell carrier with different APNs for MMS messages versus general internet data.  The intent is to allow the phone to be configured for general internet use by default and yet still get an alert when a MMS message is pending.  The user would then switch to the MMS APN to receive the message and maybe reply, then switch back to the non-MMS APN.

## Disclaimer

I developed this on a Pinephone running Mobian but one person has used an older version on a Librem 5.  It should be easy to adapt to other platforms if required.  I'm not an expert at any of this and know the code could be improved.  It could also be simpler.  If you don't care about logging, for example, about a third of the code can be removed, and mms-monitor.conf deleted.  I created this for the en_US locale and don't know what might be needed to use it in a different one, beyond translating messages.

## How it works

My understanding of the behind-the-scenes stuff is that receiving a MMS message begins with receiving a SMS that basically contains a URL for the MMS contents.  Modemmanager sees that the SMS is the prelude to a MMS and writes control data to ~/.cache/mmstng/modemmanager/.  MMSD-tng watches that directory and will attempt to process things further but can't if it's not connected to the right APN.

MMS-Monitor runs as a user service and is configured to trigger every 10 minutes (while the phone is on).  It looks for files in ~/.cache/mmstng/modemmanager/ and issues a notification if any are found.  Technically, the timer fires when the clock's minutes are evenly divisible by 10.  Perhaps different timing might work better for certain usage patterns.

Since the timer is configured with Persistent=true it will trigger when the phone awakes if it senses that an interval was missed.  That means when an incoming MMS wakes the phone the monitor will also run.  Note the sleep(5) statement on line 59 of main.py.  That is there so that when the phone wakes in this circumstance modemmanager will have enough time to write the control data before MMS-Monitor looks for it.  If you find that the phone wakes but doesn't alert you to an incoming MMS until a normal timer event several minutes later this delay should be increased.

## Switching APNs

One way to switch the APN is in gnome-settings.  A simpler-in-the-long-run way that requires a bit of do-it-yourself is to use a menu on the desktop.  If you long-press the MMS-Monitor icon you will see 2 additional actions on the pop-up menu: "APN: Internet" and "APN: MMS".  These each run a script in a terminal window.  You'll have to create the script yourself and maybe change the name of the terminal app to fit your situation.  A terminal is needed so you can respond to sudo.  Look in treebeard.MMSmonitor.desktop and you'll see what it's doing.

My script to switch from Internet to MMS on Verizon is basically:
```
sudo nmcli conn down vzwinternet
sudo nmcli conn up vzwapp
```
and switching from MMS back to Internet is: 
``` 
sudo nmcli conn down vzwapp
sudo nmcli conn up vzwinternet
```
You'll need to find the correct connection names from `nmcli conn show` or maybe by sifting through /etc/NetworkManager/system-connections/.  Strictly speaking, the "conn down" commands are not really needed but cause no harm and might prevent future trouble.

One thing I have seen (with mobian on pinephone) is if the phone goes to sleep while set to the MMS APN then when it wakes up there may be no data connection and I have to re-establish the connection with the desired APN.  I always switch back to the non-MMS connection before the phone sleeps.

A second note is that switching the APN via the scripts does not change the setting visible in gnome-settings.


## Installation

You can download a [zip file](https://gitlab.com/Huorn/mms-monitor/-/archive/main/mms-monitor-main.zip) of the latest version, clone the repository, or just download the files one at a time.  You can even copy & paste the contents.

Place the files as indicated:

~/bin/mms-monitor/
- main.py
- mms-monitor.conf

~/.config/systemd/user/
- mmsmonitor.service
- mmsmonitor.timer

~/.local/share/applications/
- treebeard.MMSmonitor.desktop

The user id is hard-coded as "mobian" so change as needed in these files:
- main.py
- mms-monitor.conf
- mmsmonitor.service
- treebeard.MMSmonitor.desktop

Ensure you have installed the gir1.2-lfb-0.0 package.

Register and enable the services (not as root):
```
$ systemctl --user enable ~/.config/systemd/user/mmsmonitor.service
$ systemctl --user enable --now ~/.config/systemd/user/mmsmonitor.timer
```

Log data will be written to ~/bin/mms-monitor/mms-monitor.log .  This can be useful initially to see when the process is running and what it finds.

## Verifying operation

You can temporarily change the target directory on lines 39 & 40 of main.py so MMS-Monitor checks for files in /tmp and it should then trigger a notification every time it is run.  The desktop icon can be used to run the process manually.

## Roadmap

The best case is we get proper dual APN support and this mess can go away.  Barring that, I think it might be much simpler for a version of this monitor to be launched via autostart and run continuously in the background, sleeping for some period between checks.  That eliminates the service files and dependence on systemd, but might lose the ability to check right after the phone awakens.

## License

This project is licensed under [Creative Commons Zero v1.0 Universal](https://creativecommons.org/share-your-work/public-domain/cc0).

## Finally

Michael R, aka Huorn on gitlab.com, and Treebeard in the Purism and Pine64 forums.

https://gitlab.com/Huorn/mms-monitor

Feb 10, 2023
