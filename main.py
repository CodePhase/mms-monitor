#! /usr/bin/env python3
"""Provide notification of queued MMS mesages."""
# Version 3

import os
import sys
__wholename = os.path.join(os.getcwd(), sys.argv[0])
__mypath, __myname = os.path.split(__wholename)
import time
import logging, logging.config, logging.handlers
__configfile = os.path.join(__mypath, "mms-monitor.conf")
try:
    logging.config.fileConfig(__configfile)
except KeyError as e:
    raise RuntimeError("The application config file may be missing or improperly formatted.", e)
_log = logging.getLogger("main")
_log.debug("begin main")

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('Lfb', '0.0')
from gi.repository import Gtk, Gio, Lfb


# https://stackoverflow.com/questions/69154975/why-is-gio-notification-not-showing-in-python
# and https://source.puri.sm/Librem5/feedbackd/-/blob/main/examples/example.py
class App(Gtk.Application):

    def __init__(self, *args, **kwargs):
        Gtk.Application.__init__(self, *args, application_id="treebeard.MMSmonitor", **kwargs)
        # Some docs say the name of the desktop file (treebeard.MMSmonitor.desktop) needs to match this
        # application id but in practice it hasn't made any apparent difference.
        self.window = None

    def do_startup(self):
        Gtk.Application.do_startup(self)

    def do_activate(self):
        targetpath = "/home/mobian/.cache/mmstng/modemmanager/"
        # targetpath = "/tmp"	# use for testing - it's never empty
        file_list = os.listdir(targetpath)
        mmscount = len(file_list)
        _log.info(f"count={mmscount}")

        if mmscount > 0:
            notification = Gio.Notification()
            # Only the title is visible for lockscreen notifications.  The body is visible after unlocking.
            notification.set_title(f"There are {mmscount} MMS messages queued.")
            # notification.set_body(f"There are {mmscount} MMS messages queued.")
            # notification.set_category("email.arrived")   #"im.received"
            # notification.set_priority(Gio.NotificationPriority.HIGH)
            self.send_notification("notification id", notification)
            Lfb.init("treebeard.MMSmonitor")
            event = Lfb.Event.new("message-new-sms")
            event.trigger_feedback()
            time.sleep(3)	# allow feedback to complete before we exit

if __name__ == "__main__":
    time.sleep(5)
    app = App()
    app.run(sys.argv)
    _log.debug("end\n")
